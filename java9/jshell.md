# jshell
> 在学习JSHELL之前确保，已经安装过jdk9，可以参阅<a href="http://blog.csdn.net/u014042066/article/details/78066968">Java9安装</a>。

# 为什么要学Jshell
> 学习Jshell需要知道他的特性， Read-Eval-Print-Loop (REPL)

- 交互式解释器（REPL）既可以作为一个独立的程序运行，
也可以很容易地包含在其他程序中作为整体程序的一部分使用。
REPL为运行Java脚本与查看运行结果提供了一种交互方式，
通常REPL交互方式可以用于调试、测试以及试验某种想法。

# 开始使用
- 切换jdk环境

![切换jdk环境](img/jdkpath.png) 

- 检查环境 

![检查环境](img/jdk_version.png) 

- 切换到Jshell环境

![切换到Jshell环境](img/jshell.png) 

- Hello World

![Hello](img/jshell_hello.png) 

# 命令
  - 输入-help 进行查看,会展示出Jshell所支持的命令

    ![Hello](img/jshell_help.png) 
    
  - /history，查看在Jshell输入的的所有指令(不管正确与否)
  
    ![Hello](img/jshell_history.png) 

 - /list,列出所有你输入过的Java源代码(非命令相关)
 
    ![Hello](img/jshell_list.png)   
    
    也可以根据id进行查询
    
    ![Hello](img/jshell_list2.png)  
     
    > 上图列出了我历史操作的一些与Jshell交互的Java代码命令。
 
    > 根据ID查看Java命令并执行。
    
    ![Hello](img/jshell_list3.png)   
 
 - /exit ,退出Jshell命令行
 
    ![Hello](img/jshell_exit.png) 

 - /edit,根据id修改曾经输入过的Java代码，一般配合/list进行使用

   > 利用List命令查找你要修改代码的Id。
   
    ![Hello](img/jshell_list_edit1.png) 

   > 输入你要更改代码的Id，回车确认。
   
    ![Hello](img/jshell_list_edit2.png) 

   > 回车后出来如下页面。
   
    ![Hello](img/jshell_list_edit3.png) 

   > 将Java代码修改为你想要的结果，点击Accept进行保存和执行，出现如下图所示结果。默认不会自动退出，需要手动exit。
   
    ![Hello](img/jshell_list_edit4.png) 

   > 重新查看list，发现多出一个修改过的历史。
   
    ![Hello](img/jshell_list_edit5.png) 
    
 - /drop 删除Java历史操作记录   
 
    > 根据id进行删除，也可以根据名称进行删除。
    
    ![Hello](img/jshell_drop1.png) 
    
    
 - /reset,重置环境状态，会清空历史和list等。
 
   ![Hello](img/jshell_reset.png) 
    
    
 - /reload,重新启动并重置环境状态，会保留历史和list等。
 
   ![Hello](img/jshell_reload.png) 
    
    
 - /imports,查看导入的Java工具类。
 
   ![Hello](img/jshell_imports.png) 
    
 - /open,引入外部Java文件   
    > 准备外部Java文件 
    
      ![Hello](img/jshell_open.png) 
      
   > 执行外部文件
      
      ![Hello](img/jshell_open2.png) 
      
     
 - /save,保存代码片段，并执行。   
    > 以下是命令操作
    
      ![Hello](img/jshell_save.png) 
      
 
 - /env,环境属性
 
     ![Hello](img/jshell_env.png) 
    
 - /vars,列出已经声明的变量和值
 
     ![Hello](img/jshell_var.png) 
    
# 常用示例    
   - 创建class
   
     ![Hello](img/jshell_class.png) 
   
   - 查看class,/types
   
     ![Hello](img/jshell_type.png) 
   
   
   - 创建method
   
     ![Hello](img/jshell_method.png) 
   
   
   - 查看method，/method
   
     ![Hello](img/jshell_method2.png) 
   
   - 执行method
   
     ![Hello](img/jshell_method3.png) 
   
   - 修改method，修改类也是一样。
   
     ![Hello](img/jshell_method4.png) 
   
   
   
   
   - 设置编辑器，利用编辑器进行编辑。
    
     >修改完毕之后，ctrl+s即可打印出类已经修改之类的提示。
   
     ![Hello](img/jshell_editor.png) 
   
# 结语

ricky   

交流群：244930845
   
   
    
 
 
