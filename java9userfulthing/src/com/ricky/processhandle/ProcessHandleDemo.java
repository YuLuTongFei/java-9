package com.ricky.processhandle;

public class ProcessHandleDemo {
    public static void main(String[] args) {
        System.out.println("PID:"+ProcessHandle.current().pid());
        System.out.println("info:"+ProcessHandle.current().info());
        System.out.println("子进程:"+ProcessHandle.current().children());
        System.out.println("是否存活:"+ProcessHandle.current().isAlive());
        System.out.println("父进程:"+ProcessHandle.current().parent());
        System.out.println("快照:"+ProcessHandle.current().descendants());
    }
}
