package com.ricky.other;

import java.util.Properties;
import java.util.Set;

/**
 * 系统中多了一些模块化的属性
 *

 sun.desktop-----windows
 awt.toolkit-----sun.awt.windows.WToolkit
 java.specification.version-----9
 file.encoding.pkg-----sun.io
 sun.cpu.isalist-----amd64
 sun.jnu.encoding-----GBK
 java.class.path-----
 java.vm.vendor-----Oracle Corporation
 sun.arch.data.model-----64
 user.variant-----
 java.vendor.url-----http://java.oracle.com/
 user.timezone-----
 os.name-----Windows 7
 java.vm.specification.version-----9
 sun.java.launcher-----SUN_STANDARD
 user.country-----CN
 sun.boot.library.path-----D:\Program Files\Java\jdk-9\bin
 sun.java.command-----java9userfulthing/com.ricky.other.SystemProperties
 jdk.debug-----release
 sun.cpu.endian-----little
 user.home-----C:\Users\temp
 user.language-----zh
 java.specification.vendor-----Oracle Corporation
 jdk.module.path-----C:\Users\temp\IdeaProjects\java-9\out\production\java9userfulthing
 java.home-----D:\Program Files\Java\jdk-9
 file.separator-----\
 java.vm.compressedOopsMode-----32-bit
 line.separator-----

 java.specification.name-----Java Platform API Specification
 java.vm.specification.vendor-----Oracle Corporation
 java.awt.graphicsenv-----sun.awt.Win32GraphicsEnvironment
 jdk.module.main.class-----com.ricky.other.SystemProperties
 jdk.module.main-----java9userfulthing
 user.script-----
 sun.management.compiler-----HotSpot 64-Bit Tiered Compilers
 java.runtime.version-----9+181
 user.name-----baiguantao
 path.separator-----;
 os.version-----6.1
 java.runtime.name-----Java(TM) SE Runtime Environment
 file.encoding-----UTF-8
 java.vm.name-----Java HotSpot(TM) 64-Bit Server VM
 java.vendor.url.bug-----http://bugreport.java.com/bugreport/
 java.io.tmpdir-----C:\Users\temp\AppData\Local\Temp\
 java.version-----9
 user.dir-----C:\Users\temp\IdeaProjects\java-9
 os.arch-----amd64
 java.vm.specification.name-----Java Virtual Machine Specification
 java.awt.printerjob-----sun.awt.windows.WPrinterJob
 sun.os.patch.level-----Service Pack 1
 java.library.path-----D:\Program Files\Java\jdk-9\bin;C:\Windows\Sun\Java\bin;C:\Windows\system32;C:\Windows;C:\ProgramData\Oracle\Java\javapath;D:\BaiduNetdiskDownload\jdk1.8.0_45\bin;C:\Program Files (x86)\Intel\iCLS Client\;C:\Program Files\Intel\iCLS Client\;C:\Windows\system32;C:\Windows;C:\Windows\System32\Wbem;C:\Windows\System32\WindowsPowerShell\v1.0\;C:\Program Files\Intel\Intel(R) Management Engine Components\DAL;C:\Program Files\Intel\Intel(R) Management Engine Components\IPT;C:\Program Files (x86)\Intel\Intel(R) Management Engine Components\DAL;C:\Program Files (x86)\Intel\Intel(R) Management Engine Components\IPT;D:\Program Files\SlikSvn\bin;C:\Program Files\Redis\;.
 java.vm.info-----mixed mode
 java.vendor-----Oracle Corporation
 java.vm.version-----9+181
 sun.io.unicode.encoding-----UnicodeLittle
 java.class.version-----53.0


 */
public class SystemProperties {
    public static void main(String[] args) {
       Properties properties= System.getProperties();
        Set<Object> sets=  properties.keySet();
        sets.forEach(key->{
           Object val= properties.get(key);
            System.out.print(key+"-----"+val+"\n");
        });
    }
}
