import com.ricky.inter.LogInter;
import ricky.impl.LogImpl;

module rootmodule {
    //引入Java9的httpclient依赖
    requires jdk.incubator.httpclient;
    // 对外提供接口工具 需要精确到具体包名
    exports com.ricky.utils;
    //只开放给onelevle_01 其他人访问不到
    exports com.ricky.entity to onelevel_01;

    exports com.ricky.inter;

    //提供抽象类loginter默认的实现为logimpl  一般联合ServiceLoader使用
    provides com.ricky.inter.LogInter with ricky.impl.LogImpl;
}