package ricky.impl;

import com.ricky.inter.LogInter;

public class LogImpl implements LogInter{
    @Override
    public void info(Object o) {
        System.out.println(this.getClass().getName()+o);
    }

    @Override
    public void error(Object o) {
        System.out.println(this.getClass().getName()+o);
    }

    @Override
    public void debug(Object o) {
        System.out.println(this.getClass().getName()+o);
    }
}
