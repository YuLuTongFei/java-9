# 说明

## 模块化示例
> rootmodule 和onelevel_01、onelevel_01是module示例代码

##  SpringBootWithJava9
> java9 环境下的SpringBoot 的demo。不过官方是2.0才支持Java9

## Java9
> 内置jshell，httpclient，module说明等一系列文章。

 - [jshell](java9/jshell.md)
 - [httpclient](java9/java9httpclient.md)
 - [模块化解惑](java9/Java9ModularIntroduce.md)
 - [琐碎特性](java9/Java9Other.md)
